import { Injectable } from '@angular/core';
import { CheckInDTO } from '../models/check-in-dto.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public baseUrl = "https://ereception.cfapps.eu10.hana.ondemand.com";

  constructor(
    private httpClient: HttpClient,
  ) { }

  public postCheckIn(data: CheckInDTO): Observable<string> {
    return this.httpClient.post<string>(this.baseUrl + '/schalter/checkInSchalter', data);
  }

  public postCheckOut(id: string): Observable<boolean> {
    return this.httpClient.delete<boolean>(this.baseUrl + '/schalter/checkOut/' + id);
  }

  public getNotfallliste(): Observable<CheckInDTO[]> {
    return this.httpClient.get<CheckInDTO[]>(this.baseUrl + '/schalter/alleBesuche');
  }

  public getSendeNotfallliste(): Observable<boolean> {
    return this.httpClient.get<boolean>(this.baseUrl + '/schalter/sendeNotfallliste');
  }
}
