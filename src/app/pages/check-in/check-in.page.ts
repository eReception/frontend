import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.page.html',
  styleUrls: ['./check-in.page.scss'],
})
export class CheckInPage implements OnInit {
  public checkInCode: string;
  public checkInFormular: FormGroup;
  public suFormular: FormGroup;
  public zeigeSicherheitsunterweisung = false;
  public zeigeValidationsFehler = false;
  public validation_messages = {
    vorname: [
      { type: 'required', message: 'Vorname muss eingegeben werden.' },
    ],
    nachname: [
      { type: 'required', message: 'Nachname muss eingegeben werden.' },
    ],
    firma: [
        { type: 'required', message: 'Firma muss eingegeben werden.' },
      ],
      eMailAdresse: [
      { type: 'required', message: 'E-Mail-Adresse muss eingegeben werden.' },
      { type: 'email', message: 'Bitte geben Sie eine gültige E-Mail-Adresse ein.' },
    ],
    eMailAdresseGastgeber: [
      { type: 'required', message: 'E-Mail-Adresse des Gastgebers muss eingegeben werden.' },
      { type: 'email', message: 'Bitte geben Sie eine gültige E-Mail-Adresse ein.' },
    ],
    istSUAngeschaut: [
      { type: 'required', message: 'Sie müssen die Sicherheitsunterweisung anschauen und verinnerlichen bevor Sie einchecken können.' },
    ],
  };

  constructor(
    private alertController: AlertController,
    private formBuilder: FormBuilder,
    private httpService: HttpService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.checkInFormular = this.formBuilder.group({
      anrede: ['Herr'],
      vorname: ['', Validators.required],
      nachname: ['', Validators.required],
      firma: ['', Validators.required],
      eMailAdresse: ['', Validators.compose([Validators.required, Validators.email])],
      eMailAdresseGastgeber: ['', Validators.compose([Validators.required, Validators.email])],
      istSUBekannt: [false],
    });
    this.suFormular = this.formBuilder.group({
      istSUAngeschaut: [false, Validators.required],
    });
   }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.checkInCode = this.router.getCurrentNavigation().extras.state.code;
      } else {
        this.router.navigate(['check-in-code']);
      }
    });
  }

  public bestaetigeEingaben() {
    if (this.checkInFormular.invalid) {
      this.zeigeValidationsFehler = true;
    } else {
      this.zeigeValidationsFehler = false;
      if (this.checkInFormular.value.istSUBekannt) {
        this.sendeEingaben();
      } else {
        this.zeigeSicherheitsunterweisung = true;
      }
    }
  }

  public bestaetigeUnterweisung() {
    if (this.suFormular.value.istSUAngeschaut) {
      this.sendeEingaben();
    } else {
      this.zeigeValidationsFehler = true;
      this.zeigeSicherheitsunterweisung = true;
    }
  }

  private sendeEingaben() {
    const formData = this.checkInFormular.value;
    const data = {
      code: this.checkInCode,
      gastgeber: formData.eMailAdresseGastgeber,
      besucher: {
          anrede: formData.anrede,
          vorname: formData.vorname,
          nachname: formData.nachname,
          firma: formData.firma,
          eMail: formData.eMailAdresse,
      }
    };
    
    this.httpService.postCheckIn(data).subscribe(
    (data) => {
      console.log(data);
      this.zeigeErfolgreichAlert();
    },
    (error) => {
      this.zeigeUnerfolgreichAlert();
    });
  }

  private async zeigeErfolgreichAlert() {
    const alert = await this.alertController.create({
      header: 'CheckIn Erfolgreich',
      message: 'Ihr CheckIn war erfolgreich, bitte Melden Sie sich beim Empfangspersonal!',
      buttons: [
        {
          text: 'Zum Start',
          handler: () => {
            this.router.navigate(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }

  private async zeigeUnerfolgreichAlert() {
    const alert = await this.alertController.create({
      header: 'Fehler!',
      message: 'Es ist ein fehler aufgetreten, bitte Melden Sie sich beim Empfangspersonal!',
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Zum Start',
          handler: () => {
            this.router.navigate(['/home']);
          }
        }
      ]
    });

    await alert.present();
  }
}
