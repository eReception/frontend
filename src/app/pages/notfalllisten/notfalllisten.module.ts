import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotfalllistenPageRoutingModule } from './notfalllisten-routing.module';

import { NotfalllistenPage } from './notfalllisten.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotfalllistenPageRoutingModule
  ],
  declarations: [NotfalllistenPage]
})
export class NotfalllistenPageModule {}
