import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotfalllistenPage } from './notfalllisten.page';

describe('NotfalllistenPage', () => {
  let component: NotfalllistenPage;
  let fixture: ComponentFixture<NotfalllistenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotfalllistenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotfalllistenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
