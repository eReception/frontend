import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotfalllistenPage } from './notfalllisten.page';

const routes: Routes = [
  {
    path: '',
    component: NotfalllistenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotfalllistenPageRoutingModule {}
