import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { CheckInDTO } from 'src/app/models/check-in-dto.model';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-notfalllisten',
  templateUrl: './notfalllisten.page.html',
  styleUrls: ['./notfalllisten.page.scss'],
})
export class NotfalllistenPage implements OnInit {
  public notfallliste: CheckInDTO[] = [];

  constructor(
    private httpService: HttpService,
    private toastController: ToastController,
  ) { }

  ngOnInit() {
    this.ladeNotfallliste()
  }

  public checkOut(id: string){
    this.httpService.postCheckOut(id).subscribe( (response) => {
      if (response) {
        this.ladeNotfallliste();
        this.zeigeErfolgreichenCheckout();
      } else {
        this.zeigeFehlgeschlagenenCheckout();
      }
    });
  }

  private ladeNotfallliste(): void {
    this.httpService.getNotfallliste().subscribe((data: CheckInDTO[]) => {
      this.notfallliste = data;
    });
  }

  public sendeNotfallliste() {
    this.httpService.getSendeNotfallliste().subscribe( (response) => {
      if (response) {
        this.ladeNotfallliste();
        this.zeigeErfolgreichGesendet();
      } else {
        this.zeigeFehlgeschlagenesSenden();
      }
    });
  }

  private async zeigeErfolgreichenCheckout() {
    const toast = await this.toastController.create({
      message: 'Der CheckOut wurde erfolgreich gespeichert!',
      position: 'top',
      duration: 2000,
      color: 'warning',
    });
    toast.present();
  }

  private async zeigeFehlgeschlagenenCheckout() {
    const toast = await this.toastController.create({
      message: 'Beim CheckOut ist ein Fehler aufgetreten. Bitte versuchen Sie es nochmal!',
      position: 'top',
      duration: 2000,
      color: 'warning',
    });
    toast.present();
  }

  private async zeigeErfolgreichGesendet() {
    const toast = await this.toastController.create({
      message: 'Die Notfallliste wurde erfolgreich gesendet!',
      position: 'top',
      duration: 2000,
      color: 'warning',
    });
    toast.present();
  }

  private async zeigeFehlgeschlagenesSenden() {
    const toast = await this.toastController.create({
      message: 'Beim senden der Notfallliste ist ein Fehler aufgetreten. Bitte versuchen Sie es nochmal!',
      position: 'top',
      duration: 2000,
      color: 'warning',
    });
    toast.present();
  }

}
