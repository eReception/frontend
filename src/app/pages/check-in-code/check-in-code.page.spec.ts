import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckInCodePage } from './check-in-code.page';

describe('CheckInCodePage', () => {
  let component: CheckInCodePage;
  let fixture: ComponentFixture<CheckInCodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInCodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckInCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
