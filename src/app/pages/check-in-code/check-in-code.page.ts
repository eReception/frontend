import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-check-in-code',
  templateUrl: './check-in-code.page.html',
  styleUrls: ['./check-in-code.page.scss'],
})
export class CheckInCodePage implements OnInit {

  constructor(
    private router: Router,
    private toastController: ToastController,
  ) { }

  ngOnInit() {
  }

  public goToCheckIn(code: string) {
    if (code && code.length > 0) {
      const navigationExtras: NavigationExtras = {
        state: {
          code
        }
      };
      this.router.navigate(['check-in'], navigationExtras);
    } else {
      this.zeigeRequiredToast();
    }
  }

  async zeigeRequiredToast() {
    const toast = await this.toastController.create({
      message: 'Bitte geben Sie einen CheckIn-Code ein!',
      position: 'top',
      duration: 2000,
      color: 'warning',
    });
    toast.present();
  }
}
