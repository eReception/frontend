import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckInCodePageRoutingModule } from './check-in-code-routing.module';

import { CheckInCodePage } from './check-in-code.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CheckInCodePageRoutingModule
  ],
  declarations: [CheckInCodePage]
})
export class CheckInCodePageModule {}
