import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CheckInCodePage } from './check-in-code.page';

const routes: Routes = [
  {
    path: '',
    component: CheckInCodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CheckInCodePageRoutingModule {}
