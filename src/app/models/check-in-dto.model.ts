export interface CheckInDTO {
    id?: number;
    code?: string;
    gastgeber?: string;
    checkInDatum?: string;
    checkInUhrzeit?: string;
    besucher: {
        anrede?: string;
        vorname?: string;
        nachname?: string;
        firma?: string;
        eMail?: string;
    };
}
